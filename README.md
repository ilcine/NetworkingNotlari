# Networking Notes:
 
Bir Network(ağ) da, iki veya daha fazla cihazın(bilgisayar,yazıcı,Router,Switch,Hub,
IP kamera vb.) bir biri ile kablolu veya kablosuz(wifi) ortamda bağlı olduğu, 
bir birileri ile veri paylaştıkları yapılardır. 
Ağ üzerinde; dosya, resim, video transferleri veya VoIP ile telefon görüşmeleri yapılabilinir.
Ağ sistemleri, yazılım programları ile sunucu ve istemciler arasında bağlantı kurar.

Bu notlarda genel kullanımı olan network konfigurasyon notları vardır.	

> _Emrullah İLÇİN_

* [IP Notları](/src/ip_notes.md)
* [IPv4 Mask](/src/ipv4_mask.md)	 
* [IPv6 mask](/src/ipv6_mask.md)

* [Expect Kullanımı](/src/expect.md)


